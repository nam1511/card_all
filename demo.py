import cv2
import os
from step1.box_card import box_card
from step2.Align import Align
from step3.box_line.get_line import box_line
path = "../data/"
def demo(path):
    dirs = os.listdir(path)
    model_card = box_card()
    model_corner = Align()
    model_line = box_line()

    dirs = [dir for dir in dirs if "jpg" in dir]
    for dir in dirs:
        img = cv2.imread(os.path.join(path, dir))
        cv2.imshow("img", img)
        cv2.waitKey(0)
        imgs_crop = model_card.crop(img)
        if len(imgs_crop) > 0:
            for img_crop in imgs_crop:
                al = model_corner.aligns(img_crop)
                lines = model_line.crop(al)
                for line in lines:
                    cv2.imshow("img", line)
                    cv2.waitKey(0)

demo(path)