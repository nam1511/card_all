import cv2
from step2.tools import four_point_transform
import numpy as np
from step2.src_bo_tro import *
from models.model import center_net
from parameter import path_model_corner


class Align():
    def __init__(self):
        self.model_corner = center_net(path_model_corner)
        self.pad_crop = 0.05
    def point_for_align(self, img):
        h, w, _ = img.shape
        pad = int(0.1 * w)
        Black = [0, 0, 0]
        img = cv2.copyMakeBorder(img, pad, pad, pad, pad, cv2.BORDER_CONSTANT, value=Black)
        h_orginnal, w_orginnal, _ = img.shape
        boxes_corner = self.model_corner.run(img)["results"]
        centers_box_corner = []
        for box_corner in boxes_corner:
            center = (int((box_corner[0] + box_corner[2]) / 2), int((box_corner[1] + box_corner[3]) / 2))
            centers_box_corner.append(center)
        cornersPad = [(0, 0), (w_orginnal, 0), (w_orginnal, h_orginnal), (0, h_orginnal)]
        centers_box_corner_distibute = distributed_point(centers_box_corner, cornersPad)
        miss = check_missing_corner(centers_box_corner_distibute)
        if len(centers_box_corner) == 4:
            if len(miss) == 0:
                return centers_box_corner
            if len(miss) == 1:
                ratio = ratio_dist_maxmin(centers_box_corner)
                if ratio > 4:
                    centers_box_corner_distibute = interpolation(centers_box_corner_distibute)
                    Four_Point = interpolation(centers_box_corner_distibute)
                    Four_Point = [Four_Point[0][0], Four_Point[1][0], Four_Point[2][0], Four_Point[3][0]]
                    return Four_Point
                if ratio < 4:
                    return centers_box_corner
            if len(miss) == 2:
                return centers_box_corner
        if len(centers_box_corner) > 4:
            if len(miss) == 0:
                Four_Point = chose_four_point(centers_box_corner_distibute, w_orginnal, h_orginnal)
                return Four_Point
            if len(miss) == 1:
                centers_box_corner_distibute = interpolation(centers_box_corner_distibute)
                Four_Point = interpolation(centers_box_corner_distibute)
                Four_Point = [Four_Point[0][0], Four_Point[1][0], Four_Point[2][0], Four_Point[3][0]]
                return Four_Point
            if len(miss) == 2:
                return cornersPad
        if len(centers_box_corner) == 3:
            if len(miss) == 1:
                Four_Point = interpolation(centers_box_corner_distibute)
                Four_Point = [Four_Point[0][0], Four_Point[1][0], Four_Point[2][0], Four_Point[3][0]]
                return Four_Point
            if len(miss) == 2:
                return cornersPad
        if len(centers_box_corner) <= 2:
            return cornersPad

    def aligns(self, img):
        h, w, _ = img.shape
        pad = int(0.1 * w)
        Black = [0, 0, 0]
        img_pad = cv2.copyMakeBorder(img, pad, pad, pad, pad, cv2.BORDER_CONSTANT, value=Black)
        Four_Point = self.point_for_align(img)
        Four_Point = get_point_pad(Four_Point)
        pts = np.array(Four_Point)
        warped = four_point_transform(img_pad, pts)

        return warped





