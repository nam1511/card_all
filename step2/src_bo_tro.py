from math import acos
from scipy.spatial import distance as Dist
from math import pow, sqrt

def check_missing_corner(centers_box_corner_distibute):
    miss = []
    for idx, center_box_corner_distibute in enumerate(centers_box_corner_distibute):
        if center_box_corner_distibute == []:
            miss.append(idx)
    return miss
def interpolation(centers_box_corner_distibute):
    centers_box_corner_distibute_cp = centers_box_corner_distibute
    miss = check_missing_corner(centers_box_corner_distibute)
    for idx, center_box_corner_distibute in enumerate(centers_box_corner_distibute):
        if len(center_box_corner_distibute) > 1:
            sumz = (0, 0)
            for box in center_box_corner_distibute:
                sumz = (sumz[0] + box[0], sumz[1] + box[1])

            centers_box_corner_distibute[idx] = [(int(sumz[0] / len(center_box_corner_distibute)), int(sumz[1] / len(center_box_corner_distibute)))]
    if len(miss) > 1:
        print("over missing")
    if len(miss) == 1:
        if miss == [0]:
            interpolation = (centers_box_corner_distibute[3][0][0] - centers_box_corner_distibute[2][0][0] + centers_box_corner_distibute[1][0][0], int(centers_box_corner_distibute[3][0][1] - centers_box_corner_distibute[2][0][1] + centers_box_corner_distibute[1][0][1]))
            centers_box_corner_distibute_cp[0].append(interpolation)
        if miss == [1]:
            print(centers_box_corner_distibute[3][0][0])
            interpolation = (centers_box_corner_distibute[0][0][0] - centers_box_corner_distibute[3][0][0] + centers_box_corner_distibute[2][0][0], centers_box_corner_distibute[0][0][1] - centers_box_corner_distibute[3][0][1] + centers_box_corner_distibute[2][0][1])
            centers_box_corner_distibute_cp[1].append(interpolation)
        if miss == [2]:
            interpolation = (centers_box_corner_distibute[3][0][0] - centers_box_corner_distibute[0][0][0] + centers_box_corner_distibute[1][0][0], centers_box_corner_distibute[3][0][1] - centers_box_corner_distibute[0][0][1] + centers_box_corner_distibute[1][0][1])
            centers_box_corner_distibute_cp[2].append(interpolation)
        if miss == [3]:
            interpolation = (centers_box_corner_distibute[0][0][0] - centers_box_corner_distibute[1][0][0] + centers_box_corner_distibute[2][0][0], centers_box_corner_distibute[0][0][1] - centers_box_corner_distibute[1][0][1] + centers_box_corner_distibute[2][0][1])
            centers_box_corner_distibute_cp[3].append(interpolation)
    return centers_box_corner_distibute_cp
def ratio_dist_maxmin(centers_box_corner):#for 4 point
    min_dist = 100000000
    max_dist = -10000000
    for idx1, center_box_corner1 in enumerate(centers_box_corner):
        for idx2, center_box_corner2 in enumerate(centers_box_corner):
            if idx1 != idx2:
                dist = Dist.euclidean(centers_box_corner[idx1], centers_box_corner[idx2])
                if dist > max_dist:
                    max_dist = dist
                if dist < min_dist:
                    min_dist = dist
    ratio = max_dist/min_dist
    return ratio
def distributed_point(centers_box_corner, corners_pad):
    centers_box_corner_distibute = [[], [], [], []]
    for i, center_box in enumerate(centers_box_corner):
        minDis = 100000
        for j, conner_pad in enumerate(corners_pad):
            Dis = Dist.euclidean(center_box, conner_pad)
            if Dis < minDis:
                minDis = Dis
                index_pad_min = j
        centers_box_corner_distibute[index_pad_min].append(center_box)

    return centers_box_corner_distibute
def chose_four_point(centers_box_corner_distibute, w_orginnal, h_orginnal):
    tl = (0, 0)
    tr = (w_orginnal, 0)
    br = (w_orginnal, h_orginnal)
    bl = (0, h_orginnal)
    min_angle = 10000
    for index_tl, center_box_corner_distibute_tl in enumerate(centers_box_corner_distibute[0]):
        for index_tr, center_box_corner_distibute_tr in enumerate(centers_box_corner_distibute[1]):
            for index_bl, center_box_corner_distibute_bl in enumerate(centers_box_corner_distibute[3]):
                for index_br, center_box_corner_distibute_br in enumerate(centers_box_corner_distibute[2]):
                    vector_t = (center_box_corner_distibute_tl[0] - center_box_corner_distibute_tr[0],
                                center_box_corner_distibute_tl[1] - center_box_corner_distibute_tr[1])
                    vector_t = (vector_t[0] / (sqrt(pow(vector_t[0], 2) + pow(vector_t[1], 2))),
                                vector_t[1] / (sqrt(pow(vector_t[0], 2) + pow(vector_t[1], 2))))


                    vector_b = (center_box_corner_distibute_bl[0] - center_box_corner_distibute_br[0],
                                center_box_corner_distibute_bl[1] - center_box_corner_distibute_br[1])
                    vector_b = (vector_b[0] / (sqrt(pow(vector_b[0], 2) + pow(vector_b[1], 2))),
                                vector_b[1] / (sqrt(pow(vector_b[0], 2) + pow(vector_b[1], 2))))



                    cosz = (vector_t[0]*vector_b[0] + vector_t[1]*vector_b[1])/(
                            sqrt(vector_t[0]*vector_t[0]+vector_t[1]*vector_t[1])*sqrt(vector_b[0]*vector_b[0] + vector_b[1]*vector_b[1] ))-0.000000001

                    angle = int(acos(cosz)*180/3.14)
                    if angle < min_angle:
                        min_angle = angle
                        tl = center_box_corner_distibute_tl
                        tr = center_box_corner_distibute_tr
                        br = center_box_corner_distibute_br
                        bl = center_box_corner_distibute_bl
    Four_Point = [tl, tr, br, bl]
    return Four_Point
def solve_point(pce,pco,k):
    point = (k*pco[0]-(k-1)*pce[0],k*pco[1]-(k-1)*pce[1])
    return point
def get_point_pad(Four_Point):
    k = 1.05

    center_pol = (int((Four_Point[0][0]+Four_Point[1][0]+Four_Point[2][0]+Four_Point[3][0])/4),
                  int((Four_Point[0][1]+Four_Point[1][1]+Four_Point[2][1]+Four_Point[3][1])/4))
    Four_Point_pad = [solve_point(center_pol, Four_Point[0], k), solve_point(center_pol, Four_Point[1], k),
                      solve_point(center_pol, Four_Point[2], k), solve_point(center_pol, Four_Point[3], k)]

    return Four_Point_pad
