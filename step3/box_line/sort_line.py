from step3.box_line.dfs import Graph

def check_interaction(box1, box2):
    sumz = (box1[3]-box1[1]) + (box2[3]-box2[1])
    uni = max(box1[3], box1[1], box2[3], box2[1])- min(box1[3], box1[1], box2[3], box2[1])
    if sumz < uni:
        interaction = 0
    if sumz >= uni:
        interaction =(sumz/uni)-1
    return interaction
def min_idx(box_group):
    minz = 100000
    idxz = 100000
    for idx, box in enumerate(box_group):
        if box[0] < minz:
            minz = box[0]
            idxz = idx
    return minz, idxz
def sort_first(val):

    return val[0]

def sort_line(rects):

    if len(rects)>0:
        graph= Graph(rects)
        box_groups = graph.group_box()
        line_boxes = []
        for box_group in box_groups:
            ts = []
            ls = []
            bs = []
            rs = []
            for box in box_group:
                ts.append(box[1])
                ls.append(box[0])
                bs.append(box[3])
                rs.append(box[2])
            line_box = [min(ls), min(ts), max(rs), max(bs)]
            line_boxes.append(line_box)
    return line_boxes
