from parameter import path_model_line
from models.model import center_net
from step3.box_line.sort_line import sort_line

def to_list_box(results_model):
    boxes = []
    for result_model in results_model:
        boxes.append([int(result_model[0]), int(result_model[1]), int(result_model[2]), int(result_model[3])])
    return boxes

class box_line():

    def __init__(self):
        model_line = center_net(path_model_line)
        self.model_card = model_line

    def crop(self, img):
        boxes = self.model_card.run(img)["results"]
        h, w, _ = img.shape
        boxes = to_list_box(boxes)
        boxes = sort_line(boxes)
        lines = []
        if len(boxes) > 0:
            for box in boxes:
                x1 = int(max(box[0], 0))
                y1 = int(max(box[1], 0))
                x2 = int(min(box[2], w))
                y2 = int(min(box[3], h))
                crop_img = img[y1:y2, x1:x2]
                lines.append(crop_img)
        return lines






