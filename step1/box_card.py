from parameter import path_model_card
from models.model import center_net

class box_card():
    def __init__(self):
        model_card = center_net(path_model_card)
        self.model_card = model_card
        self.pad_crop = 0.05
    def crop(self, img):
        boxes = self.model_card.run(img)["results"]
        h, w, _ = img.shape
        list_crop = []
        if len(boxes) > 0:

            for box in boxes:
                h1 = box[2] - box[0]
                w1 = box[3] - box[1]
                padx = int(self.pad_crop * w1)
                pady = int(self.pad_crop * h1)
                x1 = int(max(box[0] - padx, 0))
                y1 = int(max(box[1] - pady, 0))
                x2 = int(min(box[2] + padx, w))
                y2 = int(min(box[3] + pady, h))
                crop_img = img[y1:y2, x1:x2]
                list_crop.append(crop_img)
        return list_crop





