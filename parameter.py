import torch
path_model_card = "weight/model_card/model_card2.pth"
path_model_corner = "weight/model_corner/model_corner2.pth"
path_model_line = "weight/model_line/model_box_line3.pth"

class opts():
    def __init__(self):
        self.dataset = 'food'
        self.debug = 0
        self.debugger_theme = 'white'
        self.test_scales = [1.0]
        self.device = torch.device('cuda')
        self.fix_res = True
        self.input_h = 512
        self.input_w = 512
        self.pad = 31
        self.arch = 'dla_34'
        self.heads = {'hm':1, 'wh':2, 'reg':2}
        self.head_conv = 256
        self.mean = [0.408, 0.447, 0.47]
        self.std = [0.289, 0.274, 0.278]
        self.num_classes = 1
        self.reg_offset = True
        self.flip_test = False
        self.cat_spec_wh = False
        self.K = 100
        self.nms = False
        self.down_ratio = 4
        self.center_thresh = 0.1
        self.vis_thresh = 0.3
