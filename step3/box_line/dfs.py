from collections import defaultdict

def sort_first(val):
    return val[0]

def get_iou_min_ele(b1, b2):

    x_left = max(b1[0], b2[0])
    y_top = max(b1[1], b2[1])
    x_right = min(b1[2], b2[2])
    y_bottom = min(b1[3], b2[3])
    if x_right < x_left or y_bottom < y_top:
        return 0.0
    intersection_area = (x_right - x_left) * (y_bottom - y_top)
    bb1_area = (b1[2] - b1[0]) * (b1[3] - b1[1])
    bb2_area = (b2[2] - b2[0]) * (b2[3] - b2[1])
    min_erea = min(bb1_area, bb2_area)
    iou_min_ele = intersection_area / float(min_erea)
    return iou_min_ele

class Graph:

    def __init__(self, boxes):
        self.boxes = boxes
        self.IOU = 0.5
        self.graph = defaultdict(list)

    def addEdge(self):
        for idx1, v1 in enumerate(self.boxes):
            for idx2, v2 in enumerate(self.boxes):
                if (idx1 != idx2) and ((get_iou_min_ele(self.boxes[idx1], self.boxes[idx2])) > self.IOU):
                    self.graph[idx1].append(idx2)

    def DFSUtil(self, v, visited):
        self.addEdge()
        visited.add(v)
        for neighbour in self.graph[v]:
            if neighbour not in visited:
                self.DFSUtil(neighbour, visited)

    def DFS(self, v):
        visited = set()
        self.DFSUtil(v, visited)
        self.visited = visited
        return self.visited

    def group_box(self):
        boxes_group = []
        for idx in range(len(self.boxes)):
            a = list(self.DFS(idx))
            boxes_group.append(a)
        for box_group in boxes_group:
            box_group.sort()
        res = []
        [res.append(x) for x in boxes_group if x not in res]
        boxes_group = []
        for i, box_group_idx in enumerate(res):
            box_group = [self.boxes[j] for j in box_group_idx ]
            boxes_group.append(box_group)
        return boxes_group



